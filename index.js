const default_reject = (req, res, next, invalid) => res.sendStatus(403)
const default_doc = { path: '/_interface', public: false }

module.exports = ({
  types = {}, aliases = {}, intf,
  base_path = '', ignore_method,
  reject = default_reject,
  doc = default_doc,
  debugging
}) => (req, res, next) => {
  
  const method = req.method.toLowerCase()
  let path = req.originalUrl.split('?')[0]
  if(base_path) {
    if(!path.startsWith(base_path)) throw Error(`path does not start with base_path (path: ${path}, base_path: ${base_path})`)
    path = path.substr(base_path.length)
  }
  
  if(method === 'get') {
    const ip = req.connection?.remoteAddress?.replace(/^.*:/, '')
    if(
      doc.path === path &&
      (doc.public || ['1', '127.0.0.1'].includes(ip))
    ) {
      const params = { types, aliases, intf, ignore_method }
      return require('./doc')(params)(req, res, next)
    }
    return next()
  }
  
  const _next = err => {
    if(err) return next(err)
  
    const body = JSON.parse(JSON.stringify(req.body))
  
    const validate = (type_defs, values, cb) => {
      for(const key in type_defs){
        let type_def = type_defs[key]
        if(typeof type_def === 'string'){
          const alias = type_defs[key]
          if(!aliases[alias]) throw Error(`alias is not defined for '${alias}'`)
          type_def = aliases[alias]
        }
      
        const { type, args, opt, custom } = type_def
        if(!types[type]) throw Error(`type is not defined for '${type}'`)
      
        if(values[key] == null) {
          if(!opt) return cb(key, type, args, values[key])
          continue
        }
      
        if(!types[type].apply(null, [ values[key], ...(args || []) ])) return cb(key, type, args, values[key])
        if(custom && !custom(values[key], body)) return cb(key, type, args, values[key])
      }
      return true
    }
  
    
    types = {
      String: (value, min_length, max_length) => (
        typeof value === 'string' &&
        (min_length != null ? min_length <= value.length : true) &&
        (max_length ? value.length <= max_length : true)
      ),
      Number: (value, min, max, is_integer = false) => (
        typeof value === 'number' &&
        (is_integer ? Number.isInteger(value) : true) &&
        (min != null ? min <= value : true) &&
        (max != null ? value <= max : true)
      ),
      Boolean: (value) => (typeof value == 'boolean'),
      Array: function(value, type){
        if(!Array.isArray(value) || !types[type]) return false
        const args = Array.from(arguments).splice(2)
        const test = item => types[type].apply(null, [item, ...args])
        for(const item of value){
          if(!test(item)) return false
        }
        return true
      },
      JSON: (value, map) => {
        if(typeof value !== 'object') return false
        if(typeof map === 'string'){
          if(!aliases[map]) throw Error(`alias is not defined for '${map}'`)
           map = aliases[map]
        }
        return validate(map, value, (key, type, args, value) => {
          if(debugging) console.log(`[express-interface] INVALID JSON KEY-VALUE ${key}, ${JSON.stringify(value)}`)
          return false
        })
      },
      ...types
    }
    
  
    if(!ignore_method){
      for(const path in intf){
        for (const method in intf[path]){
          intf[path][method.toLowerCase()] = intf[path][method]
        }
      }
    }

    
    const _reject = (key, type, args) => {
      const invalid = { path, method, ... key ? { key, value: body[key], type, args } : {} }
      reject(req, res, next, invalid)
      return false
    }
    
    const _intf = !intf[path] ? null : ignore_method ? intf[path] : intf[path][method]
    if(!_intf || !_intf.req || !_intf.res) return _reject()
  
    const validated = validate(_intf.req, body, _reject)
    if(!validated) return
  
    res._json = res.json
    res.json = body => {
      body = JSON.parse(JSON.stringify(body || {}))
      validate(_intf.res, body, key => { throw Error(`invalid response (path: ${path}, invalid key: ${key})`) })
      res._json(body)
    }

    next()
  }
  
  req.body ? _next() : (require('body-parser')).json()(req, res, _next)
}