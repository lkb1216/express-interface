

module.exports = ({ types, aliases, intf, ignore_method }) => {
  
  const mapper = (map) => {
    const _map = {}
    for(let key in map){
      const ext = []
      let type_def = map[key]
      if(typeof type_def === 'string') {
        type_def = aliases[key]
        ext.push(`alias'${key}'`)
      }
      const { type, args, opt, custom } = type_def
      if(opt) ext.push('opt')
      if(custom) ext.push('custom')
      switch(type){
        case 'JSON': _map[key] = mapper(args[0]); break
        case 'Array': _map[key] = [ args[0] === 'JSON' ? mapper(args[1]) : args[0] ]; break
        default: _map[key] = type + (ext.length ? ` (${ext.join(', ')})` : '')
      }
    }
    return _map
  }
  
  
  return (req, res, next) => {
    const _intf = {}
    
    for (const path in intf) {
      _intf[path] = {}
      
      if (ignore_method) {
        for (const flow in intf[path]) {
          const map = intf[path][flow]
          _intf[path][flow] = mapper(map)
        }
      } else {
        for (const method in intf[path]) {
          _intf[path][method] = {}
          
          for (const flow in intf[path][method]) {
            const map = intf[path][method][flow]
            _intf[path][method][flow] = mapper(map)
          }
        }
      }
      
    }
    
    res.end(JSON.stringify(_intf, null, 2))
  }
}