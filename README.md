# express-interface
```
👻 An express middleware to implement api interface 
```
* Validates JSON request body
* Validates JSON response body
* Provides an interface document


## Install
```sh
npm install express-interface
```


## Usage
```js
const express = require('express')
const expressInterface = require('express-interface')

const app = express()

const params = {
  types: {
    'Custom': (value, arg1, arg2) => value === 'Custom'
    // define custom type validators
  },
  aliases: {
    'key3': { 
      type: 'String', args: [ 1, 3 ], 
      custom: (v, body) =>  v = body.key2[0]
    }
  },
  intf: {
    '/path': {
      post: {
        req: {
          key1: { type: 'Number', args: [ 0, 1, true ], opt: true },
          key2: {
            type: 'Array', 
            args: [ 'JSON', { key1: { type: 'String' } }], 
            custom: (v, body) => v[0] === String(body.key1)
          },
          key3: 'key3'
        },
        res: {
          key1: { type: 'Custom' }
        }
      }
    }
  },
  
  base_path: '/base',
  // ignore_method: true, 
  
  reject: (req, res, next, invalid) => {
    console.log(invalid)
    res.sendStatus(404)
  },
  
  doc: { path: '/_doc', public: false }
  // 'http://localhost:port/base/_doc' will render the interface document
}

app.use(expressInterface(params))
app.post('/base/path', (req, res, next) => res.json({ key1: 'Custom' }))

```
### expressInterface(params)

#### params.intf: Object
```
{ [path]: {
    [method]: {
      req: { [key]: { type: String[, args: Array, opt: Boolean, custom: Fn(value: Any, body: Object) => Boolean ] } | Alias: String },
      res: { [key]: { type: String[, args: Array, opt: Boolean, custom: Fn(value: Any, body: Object) => Boolean ] } | Alias: String  }
    }
  }
}
```
Decide a type for each body field to validate.   
Add <code>args: [ arg1, arg2, ... ]</code> to pass extra arguments to a validating function.  
Add <code>opt: true</code> to make the field optional.  
Add <code>custom: (value, body) => true/false</code> to run an additional validating function, which also provides all body values.   
Use <code>Alias</code> for a repetitive type definition. (refer to the <code>params.aliases</code> below) 

#### params.types(optional): Object
```
{ [CustomType]: Fn(value, arg1, arg2, ...) => Boolean }
```
Define custom types and their validating functions.   
A validating function must return <code>true</code> if its value is validated,
and return <code>false</code> if its value does not conform to the interface.

The middleware provides basic type validators as follows.
```
{
  String: Fn(value: Any, min_length: Number, max_length: Number),
  Number: Fn(value: Any, min: Number, max: Number, is_integer: Boolean = false),
  Boolean: Fn(value: Any),
  Array: FN(value: Any, type: String),
  JSON: Fn(value: Any, intf: Object)
}
```

#### paras.aliases(optional): Object
```
{ [Alias]: type: String[, args: Array, opt: Boolean, custom: Fn(value: Any, body: Object) => Boolean ] }
```
Define aliases for specific types that appear repetitively in the interface.   
Put an alias instead of defining a type repetitively.

#### params.base_path (optional): String
Define base path for api.   
This option would be particularly useful with routers.

#### params.ignore_method (optional): Boolean
Set <code>true</code> if you use only one method(e.g., POST)   
If this option is set, <code>params.intf</code> must look like below.
```
{ [path]: {
  req: { [key]: { type: String[, args: Array, opt: Boolean, custom: Fn(value: Any, body: Object) => Boolean ] } },
  res: { [key]: { type: String[, args: Array, opt: Boolean, custom: Fn(value: Any, body: Object) => Boolean ] } }
}
```

#### params.reject (optional): Fn(req, res, next, invalid)
Define reject function to handle invalid requests.   
The default reject function is as follows.
```js
const reject = (req, res, next, invalid) => res.sendStatus(403)
// invalid = { path, method[, key, value, type, args ] }
```
If a request path is not in <code>params.intf</code>, 
the function will also be called with <code>invalid</code> not containing <code>key, value, type, args</code>.

#### params.doc (optional): Object
```
{ path: String, public: Boolean }
```
Set <code>path</code> in which an interface document will be rendered.   
Set <code>public</code> to <code>true</code> if the interface document should be open to all.   
If <code>public</code> is <code>false</code>, the document page is accessible only through localhost.

The default doc is as follows.
```
{ path: '/_interface', public: false }
```


## Caution
* The middleware does not handle GET requests.
* Request body should be assgined to <code>req.body</code> before using the middleware.
* The middleware uses <code>(require('body-parser')).json()</code> to parse request body, if not assgined to <code>req.body</code> 
