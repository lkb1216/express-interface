const port = 7777
const express = require('express')
const parser = require('body-parser')
const fetch = require('node-fetch')
const expressInterface = require('./index')
const app = express()

const post = {
  req: {
    a: 'a',
    b: {opt: false, type: 'String', args: [3, 5], custom: (v, body) =>  v === 'abcd'},
    c: {opt: false, type: 'Number', args: [3, 5]},
    d: {opt: false, type: 'Array', args: ['Custom', 6]},
    e: { opt: false, type: 'JSON', args: [{
        a: {opt: true, type: 'String', args: [3, 5]},
        b: {opt: false, type: 'String', args: [3, 5]},
        c: {
          opt: false, type: 'JSON', args: [{
            a: {opt: true, type: 'String', args: [3, 5]},
            b: {opt: false, type: 'String', args: [3, 5]},
          }]
        },
      }]
    },
    f: { opt: false, type: 'Array', args: ['JSON', {
        c: {opt: false, type: 'Boolean' },
      }]
    }
  },
  res: {
    a: { opt: false, type: 'Custom', custom: v => typeof v === 'string' }
  }
}

const params = {
  types: {
    Custom: (value) => value === 'CUSTOM'
  },
  aliases: {
    a: {opt: true, type: 'String', args: [3, 5]}
  },
  intf: { '/': { ...post }, '/a': { ...post }, '/b': { ...post } },
  base_path: '/a',
  ignore_method: true,
  reject: (req, res, next, invalid) => {
    console.log(JSON.stringify(invalid, null, 2))
    res.sendStatus(404)
  },
  doc: { path: '/_doc' }
}

app.use(
  expressInterface(params)
)

app.post('/a/', (req, res) => res.json({ a: 'CUSTOM' }))
app.post('/a/a', (req, res) => res.json({ a: 'CUSTOM' }))
app.post('/a/b', (req, res) => res.json({ a: 'CUSTOM2' }))

app.listen(port, async() => {
  console.log('test server listening to', port)
  
  const body = {
    b: 'abcd',
    c: 4,
    d: [ 'CUSTOM', 'CUSTOM', 'CUSTOM' ],
    e: { a: 'abcd', b: 'abcd', c: { b: 'abcd' } },
    f: [ { c: false }, { c: true }]
  }
  
  const url = path => `http://localhost:${port}${path}`
  const options = body => ({ method: 'post', body: JSON.stringify(body), headers: { 'Content-Type': 'application/json' } })
  const report = (status, expected) => console.log(status === expected ? ' - passed' : ' - failed')
  
  {
    console.log('\ntest#1: valid request & valid response')
    const res = await fetch(url('/a/'), options(body))
    report(res.status, 200)
  }

  {
    body.c = 1
    console.log('\ntest#2: invalid request & valid response')
    const res = await fetch(url('/a/a'), options(body))
    report(res.status, 404)
  }
  
  {
    body.c = 4
    console.log('\ntest#3: valid request & invalid response')
    const res = await fetch(url('/a/b'), options(body))
    report(res.status, 500)
  }
  
  console.log('\ntest#4: check the interface here:', url(params.base_path + params.doc.path))

})
